import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NotificationCreatePageRoutingModule } from './notification-create-routing.module';

import { NotificationCreatePage } from './notification-create.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NotificationCreatePageRoutingModule,
    ComponentsModule
  ],
  declarations: [NotificationCreatePage]
})
export class NotificationCreatePageModule {}
