import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PublicationCreatePageRoutingModule } from './publication-create-routing.module';

import { PublicationCreatePage } from './publication-create.page';
import { ComponentsModule } from '../../components/components.module';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PublicationCreatePageRoutingModule,
    ComponentsModule,
    PipesModule
  ],
  declarations: [PublicationCreatePage]
})
export class PublicationCreatePageModule {}
