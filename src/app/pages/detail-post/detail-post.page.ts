import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {  User } from '../../interfaces/interfaces';
import {Location} from '@angular/common';
import { Storage } from '@ionic/storage';
import { UiServiceService } from '../../services/ui-service.service';
import { UsersService } from '../../services/users.service';
import { NavController, IonSlides, PopoverController, ModalController, IonContent } from '@ionic/angular';
import { environment } from '../../../environments/environment';
import { PopaccountComponent } from '../../components/popaccount/popaccount.component';

import { ActivatedRoute } from '@angular/router';
import { PostsService } from '../../services/posts.service';
import { ModalPayComponent } from '../../components/modal-pay/modal-pay.component';

import { SocialSharing } from '@ionic-native/social-sharing/ngx';

// declare let paypal: any;

const site = environment.site;

@Component({
  selector: 'app-detail-post',
  templateUrl: './detail-post.page.html',
  styleUrls: ['./detail-post.page.scss'],
})
export class DetailPostPage implements OnInit {

  @ViewChild('slideSecundario', {static: false}) slides: IonSlides;
  @ViewChild('scrollElement', {static: false}) content: IonContent;
  @ViewChild('quantity', {static: false}) quantity: ElementRef;

  // pageId = window.location.pathname;
  serviceArray : any[] = [];
  servicioSeleccionado = null;
  monedaEnviaSeleccionado = 'Seleccione';
  monedaRecibeSeleccionado = 'Seleccione';
  metodoPagoSeleccionado = 'Seleccione';
  paymentMethodsArray : any[] = [];

  serviceSelected = null;
  monedaSend = null;
  monedaRecepter = null;
  paymentMethodSelected = null;
  amountSend: any = 0;
  rateSelected = null;
  rate: number = 0;
  rateProduct: number = 4.5;
  newComment: string = '';

  
  amountRecept: any = 0;

  errorMessage: any = null;
  errorMessage2: any = null;

  fecha = new Date();

  slideActive:string = '0';


    mostrar:boolean = false;

    skeleton:boolean = true;

    userLoged: User = {
      name: '',
      email: ''
    };
    postId = 0;

    post : any = {};
    countrys: any[] = [];
    monedas: any[] = [];


  imgs: any[] = [];
  cortar = 600;

  slideSoloOpts = {
    allowSlideNext: false,
    allowSlidePrev: false
  }

  slideSoloOpts2 = {
    autoplay:true,
    pagination: false,
    speed: 600
  }

  constructor(
     private _location: Location,
     private _storage: Storage,
     private _uiService: UiServiceService,
     private _usersService: UsersService,
     private _postService: PostsService,
     private navController: NavController,
     private popoverController: PopoverController,
     private activatedRoute: ActivatedRoute,
     private modalCtrl: ModalController,
     private _socialSharing: SocialSharing
     ) { 
      
     }

  ngOnInit() {

    this._postService.optionSelected.subscribe( optionEmitted => {

      console.log('OptionEmitted', optionEmitted);

      switch(optionEmitted.selectedClicked) {

        case 'servicesSelect': 

        this.servicioSeleccionado = optionEmitted.option;

        this.sSelected( this.servicioSeleccionado );

        console.log('OptSelected', this.servicioSeleccionado );

        break;

        case 'monedasSendSelect': 

        this.monedaEnviaSeleccionado = optionEmitted.option;

        console.log('OptSelected', this.monedaEnviaSeleccionado );

        if (this.monedaEnviaSeleccionado != 'Seleccione' && this.monedaRecibeSeleccionado != 'Seleccione' &&  this.metodoPagoSeleccionado != 'Seleccione') {

          this.viewRate();
        }

    

        break;

        case 'monedasReceptSelect': 

        this.monedaRecibeSeleccionado = optionEmitted.option;

        console.log('OptSelected', this.monedaRecibeSeleccionado );

        if (this.monedaEnviaSeleccionado != 'Seleccione' && this.monedaRecibeSeleccionado != 'Seleccione' &&  this.metodoPagoSeleccionado != 'Seleccione') {

          this.viewRate();
        }

        break;

        case 'paymentMethodSelect': 

        this.metodoPagoSeleccionado = optionEmitted.option;

        console.log('OptSelected', this.metodoPagoSeleccionado );

        this.pSelected(this.metodoPagoSeleccionado);

        if (this.monedaEnviaSeleccionado != 'Seleccione' && this.monedaRecibeSeleccionado != 'Seleccione' &&  this.metodoPagoSeleccionado != 'Seleccione') {

          this.viewRate();
        }

        break;

      }
    });

   this.skeleton = true;

    this.activatedRoute.params.subscribe( params => {

      console.log('ID_PRODUCT', params['id']);
      this.postId = params['id'];
    });

    this._postService.getPost(this.postId).subscribe(resp => {

      console.log('PRODUCT', resp['product']);

      this.post = resp['product'];

      this.imgs.push(... this.post['image'].split(','));

      console.log('POST', this.post);
      console.log('IMGS', this.imgs);

      this.skeleton = false;

    });

    this._postService.getcountrys().subscribe( resp => {

      this.countrys = resp['data'].data;
      console.log('COUNTRYS', this.countrys);

    });

    this._postService.getmonedas().subscribe( resp => {

      this.monedas = resp['data'].data;
      console.log('MONEDAS', this.monedas);

    });

    this._usersService.getUsuario().then( data => {
      this.userLoged = data;
      console.log('USERLOG', data);
      if( this.userLoged.name == undefined) {
      // this._uiService.presentAlert('Tus comentarios son importantes. Para nosotros es un apoyo a nuestra labor.. Dejanos tu comentario', 5000 );
      }
    })
    .catch( err => {
      // this._uiService.presentAlert('Tus comentarios son importantes. Para nosotros es un apoyo a nuestra labor.. Dejanos tu comentario', 5000 );
      this.userLoged = {};
    });

  }

  ionViewDidLoad() { this.slides.lockSwipes(true) } 


  regresar() {
    // this.modalController.dismiss();
    this._location.back();
  }

  comentar() {

    if( this.userLoged.name == undefined) {
      this.mostrar = false;
      this._uiService.presentAlert('Por favor ingresa con tu cuenta o create una nueva, Haz Click en CREAR UNA CUENTA', 0 );

      this.navController.navigateRoot(`/login`, { animated: true });

      }else{
        this.mostrar = true;
      }
  }

  mostrarHistorial() {


    this.slides.lockSwipes(false);
    this.slides.slideTo(1);
    
    this.slides.lockSwipes(true);
    this.slideActive = "1";
  }

  mostrarTasa() {

    if( this.slideActive == "0" ) {
      this.regresar();
    } else {


    this.slides.lockSwipes(false);
    this.slides.slideTo(0);
    
    this.slides.lockSwipes(true);
    this.slideActive = "0";

    }
  }

  async mostrarPop( evento: any ) {

    let selectedClicked = evento.toElement.id;

    console.log('Selected clicked', evento.toElement.id);

    let optionsArray: any;

    switch( selectedClicked ) {
        case 'servicesSelect': 
 
        this.servicesSelect();
         optionsArray = this.serviceArray;

        break;

        case 'monedasSendSelect': 

          this.errorMessage2 = null;
         
          //  this.monedaSeleccionada = 'Moneda'
          optionsArray = this.monedasSelect();

        break;

        case 'monedasReceptSelect': 
         
        this.errorMessage2 = null;

        optionsArray = this.monedasSelect();

      break;

      case 'paymentMethodSelect': 

      this.errorMessage2 = null;

        optionsArray = this.paymentMethodsSelected();

        break;
    }



  

   

   const popover = await this.popoverController.create({
     component: PopaccountComponent,
     componentProps:{ optionsArray, selectedClicked },
     event: evento,
     mode: 'ios',
     backdropDismiss: true,
     cssClass: 'pop'
   });

   popover.style.cssText = '--min-width: 150px; --max-width: 320px; ';

   await popover.present();

   // const {data} = await popover.onDidDismiss();
   const {data} = await popover.onWillDismiss();
 }



  async paymentRegister() {

    if ( this.amountSend > 0 ) {

  const modal = await this.modalCtrl.create({
    component: ModalPayComponent,
    componentProps: {
      serviceSelected: this.serviceSelected,
      monedaSend: this.monedaSend,
      monedaRecepter: this.monedaRecepter,
      paymentMethodSelected: this.paymentMethodSelected,
      amountSend: this.amountSend,
      rateSelected: this.rateSelected,
      paymentMethodsArray: this.paymentMethodsArray
    }
  });

  modal.present();

} else {

  console.log('ERROR', 'Seleccione los datos correspondientes, ingrese un monto y efectue el pago');
  this.errorMessage2 = `Seleccione los datos correspondientes, ingrese un monto y efectue el pago`;

}

 }

 async servicesSelect() {

  let key: any;
  let allowsServices = this.post.service_allows;

  await allowsServices.map( ( key ) => {

    let s = key.service_type.service_name;
    let repeat = false;

    if(this.serviceArray.length > 0 ) {

      let idx;

          for ( idx in this.serviceArray) {

            console.log('VALUE', this.serviceArray[idx]);

            if( this.serviceArray[idx] === s ) {

              console.log('SERVICIO ECONTRADO');
              repeat = true;
            }

          }

      }

    if( !repeat ) {
      this.serviceArray.push(s);
    }
    
  });

  console.log(this.serviceArray);
 }

 countrysSelect() {

  let array: any[] = [];

  this.countrys.map( key => {
    array.push(key.name);
  });

 

  return array;

 }

 monedasSelect() {

  let array: any[] = [];

  this.monedas.map( key => {
    array.push(key.name);
  });

 

  return array;

 }

 paymentMethodsSelected() {

  let key: any;
  let allowsServices = this.post.service_allows;
  this.paymentMethodsArray = [];

  allowsServices.map( ( key: any ) => {

    let s = key.payment_method.payment_method_name;

    this.paymentMethodsArray.push(s);

    
  });
      console.log('PAYMENT_METHODS: ', this.paymentMethodsArray);
  return this.paymentMethodsArray;

 }

 sSelected( name: any ) {

  this.serviceSelected = null;
  let key: any;
  let allowsServices = this.post.service_allows;

  allowsServices.map( ( key: any ) => {

    let s = key.service_type.service_name;

    if( name === s ) {

      this.serviceSelected = key;

    }


    
  });
      console.log('PAYMENT_METHOD_SELECTED: ', this.paymentMethodSelected );

 }

 pSelected( name: any ) {

  this.paymentMethodSelected = null;
  let key: any;
  let allowsServices = this.post.service_allows;

  allowsServices.map( ( key: any ) => {

    let s = key.payment_method.payment_method_name;

    if( name === s ) {

      this.paymentMethodSelected = key.payment_method;

    }


    
  });
      console.log('PAYMENT_METHOD_SELECTED: ', this.paymentMethodSelected );

 }

 viewRate() {

  let rates: any[] = [];
  let monedaSendSelected_id: string;
  let monedaReceptSelected_id: string;
  this.monedaSend = null;
  this.rateSelected = null;

  this.post.service_allows.map( (key) => {

    if (key.service_type.service_name === this.servicioSeleccionado && key.rates.length > 0) {
      
      console.log('RATES_USER', key.rates);

      // Get ids contrys selected

      this.monedas.map( (key) => {

        if ( key.name === this.monedaEnviaSeleccionado) {

          monedaSendSelected_id = key.id;
          this.monedaSend = key;

        }

        if ( key.name === this.monedaRecibeSeleccionado) {

          monedaReceptSelected_id = key.id;
          this.monedaRecepter = key;

        }


      });

      // Search the rate corresponding 
      
      for (let idx in key.rates ) {

        rates.push(key.rates[idx]);

        if ( key.rates[idx].moneda_emitter_id === monedaSendSelected_id 
          && key.rates[idx].moneda_recepter_id === monedaReceptSelected_id 
          && key.rates[idx].service_allow.payment_method_id === this.paymentMethodSelected.id) {

          this.rateSelected = key.rates[idx]; 

        }

      }


    }

  });

  console.log('RATES', rates);
  console.log('RATE_SELECTED', this.rateSelected);
  console.log('MONEDA_SEND', this.monedaSend);
  console.log('MONEDA_RECEPTER', this.monedaRecepter);

 

  if ( this.monedaSend != null && this.monedaRecepter != null ) {

    console.log('ESCROLEANDO');
    //  this.content.scrollToBottom();
    // this.content.scrollToPoint(0, -500, 300);

    
    this.content.scrollToPoint(0, 465, 1000);
  
    }

 }

 calculate( value: any ) {

  this.amountSend = value;
  this.amountRecept = 0;
  this.errorMessage = null;
  this.errorMessage2 = null;

  if ( this.amountSend.match(/^([0-9])*$/) && this.amountSend != "" ) {

    console.log('VALOR', value);


      console.log('TASA', this.rateSelected);

      if ( this.amountSend <= this.rateSelected.min_quantity) {

        console.log('MONTO NO DISPONIBLE', 'El monto minimo de envio es de: '+ this.rateSelected.min_quantity);
        this.errorMessage = `El monto minimo de envio es de: ${this.rateSelected.min_quantity} ${this.monedaSend.symbol}`;
      }

      if ( this.amountSend >= this.rateSelected.quantity) {

        console.log('MONTO NO DISPONIBLE', 'El monto maximo de envio es de: '+ this.rateSelected.quantity);
        this.errorMessage = `El monto maximo de envio es de:  ${this.rateSelected.quantity} ${this.monedaSend.symbol}`;
      }

      if ( this.amountSend >= this.rateSelected.min_quantity  && this.amountSend <= this.rateSelected.quantity ) {

        this.amountRecept = this.amountSend * this.rateSelected.buy_rate;
      }

      

      console.log('RECIBES', this.amountRecept);

    




 } else {
  console.log('INGRESE SOLO NUMEROS', value);
 }

}

compartir() {
  this._socialSharing.share(`${this.post.name }
  
  ${this.post.description}`);
}

onRateChange( value: number ) {

  console.log('EVENTO: ',  value);
  this.rate = value;

}


call() {

}

sendWhatsapp() {
  
}


}
