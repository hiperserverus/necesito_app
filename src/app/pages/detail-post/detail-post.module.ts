import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailPostPageRoutingModule } from './detail-post-routing.module';

import { DetailPostPage } from './detail-post.page';

import { PipesModule } from '../../pipes/pipes.module';

import { ComponentsModule } from 'src/app/components/components.module';

import { DisqusModule } from 'ngx-disqus';

import { IonicRatingModule } from 'ionic-rating';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailPostPageRoutingModule,
    PipesModule,
    ComponentsModule,
    DisqusModule,
    ComponentsModule,
    IonicRatingModule
  ],
  declarations: [DetailPostPage]
})
export class DetailPostPageModule {}
