import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { IonSlides, NavController, ToastController } from '@ionic/angular';
import { UsersService } from '../../services/users.service';
import { UiServiceService } from '../../services/ui-service.service';
import { User } from '../../interfaces/interfaces';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  @ViewChild('slidePrincipal', {static: false}) slides: IonSlides;

  avatars = [
    {
      img: 'av-1.png',
      seleccionado: true
    },
    {
      img: 'av-2.png',
      seleccionado: false
    },
    {
      img: 'av-3.png',
      seleccionado: false
    },
    {
      img: 'av-4.png',
      seleccionado: false
    },
    {
      img: 'av-5.png',
      seleccionado: false
    },
    {
      img: 'av-6.png',
      seleccionado: false
    },
    {
      img: 'av-7.png',
      seleccionado: false
    },
    {
      img: 'av-8.png',
      seleccionado: false
    },
];

avatarSlide = {
  slidesPerview: 3.5
};

loginUser = {
  email: '',
  password: ''
}

registerUser: User = {
  dni: '',
  name: '',
  email: '',
  phone: '',
  password: '',
  password_confirmation: '',
}

slideActive:string = '0';

  constructor( private _usersService: UsersService,
                private _navController: NavController,
                private _uiService: UiServiceService) { 
  }

  ngOnInit() {
  
  }

  ionViewDidLoad() { this.slides.lockSwipes(true) } 

 login( flogin: NgForm ) {

    if( flogin.invalid ) {
      return;
    }
   this._usersService.login(this.loginUser.email, this.loginUser.password)
   .then( resp => {
    this._navController.navigateRoot('/timeline', {animated: true});
   }) 
   .catch( err => {
    this._uiService.presentToast('Usuario y contraseña no son correctos');
   });
  }

  registro( fRegistro ) {

    if ( fRegistro.invalid ) {
      return;
    }

    this._usersService.registro(this.registerUser)
    .then( resp => {
     this.mostrarLogin();
     this._uiService.presentToast('Usuario creado exitosamente!!!');
    }) 
    .catch( err => {
     this._uiService.presentToast('No se pudo crear el usuario, intente nuevamente');
    });


  }

  seleccionarAvatar(avatar) {
    this.avatars.forEach(av => av.seleccionado = false);

    avatar.seleccionado = true;
  }
  mostrarRegistro() {
    this.slides.lockSwipes(false);
    this.slides.slideTo(1);
    
    this.slides.lockSwipes(true);
    this.slideActive = "1";
  }

  mostrarLogin() {
    this.slides.lockSwipes(false);
    this.slides.slideTo(0);
    
    this.slides.lockSwipes(true);
    this.slideActive = "0";
  }

}
