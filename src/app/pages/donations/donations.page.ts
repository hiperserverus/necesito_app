import { Component, OnInit } from '@angular/core';
 import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';


@Component({
  selector: 'app-donations',
  templateUrl: './donations.page.html',
  styleUrls: ['./donations.page.scss'],
})
export class DonationsPage implements OnInit {

   site:String = "https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=Z2MBWPWFPTLNJ&currency_code=USD&source=url";
  // site:String = "https://www.marca.com";

  constructor(private iab: InAppBrowser) { }

  ngOnInit() {
    const browser = this.iab.create('https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=Z2MBWPWFPTLNJ&currency_code=USD&source=url','_blank',{location:'yes'});

  }

}
