import { Component, OnInit } from '@angular/core';
import { PostsService } from '../../services/posts.service';
import { Post } from '../../interfaces/interfaces';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.page.html',
  styleUrls: ['./timeline.page.scss'],
})
export class TimelinePage implements OnInit {

  posts: any[] = [];
  titulo:string =  'Necesi.top';
  habilitado:boolean = true;

  constructor(private _postsService: PostsService) { }

  ngOnInit() {
    this.loadData();

    this._postsService.nuevoPost.subscribe(post => {
        this.posts.unshift( post );
    });
  }
  ngOnDestroy() {
      console.log('SALIO DEL TIMELINE');
    this._postsService.pageReset();
  }

  loadData( event?, pull: boolean = false) {

    this._postsService.getPosts( pull ).subscribe( resp => {

      console.log(resp);
      this.posts.push(...resp['products'].data);

      if( event ) {
        event.target.complete();

        if ( resp['products'].data.length === 0){
              this.habilitado = false;
        }
      }


    });
  }

  doRefresh( event ) {
      this.loadData(event, true);
      this.habilitado = true;
      this.posts = [];
  }

}
