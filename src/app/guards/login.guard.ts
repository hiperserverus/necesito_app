import { Injectable } from '@angular/core';
import { CanLoad } from '@angular/router';
import { Observable } from 'rxjs';
import { UsersService } from '../services/users.service';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanLoad  {

  constructor( private _usersService: UsersService ) {}
  pase:boolean = false;

  async canLoad() {
    
  let p = await  this._usersService.validaToken()
    .then( resp => {
       this.pase = true;
    }) 
    .catch( err => {
     
        this.pase = true;
      
      
    });
    return this.pase
  }
  
}
