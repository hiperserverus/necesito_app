import { Injectable, EventEmitter, Output} from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { PostsResponse, Post, User } from '../interfaces/interfaces';
import { UsersService } from './users.service';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
const _URL = environment.url;

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  userLoged: User = null;
  paginaPosts = 0;

  @Output() optionSelected: EventEmitter<any>


  nuevoPost = new EventEmitter<Post>();
  purgarImagenes = new EventEmitter<Boolean>();
  sendDetailPost = new EventEmitter<Post>();
  sendDetailImages = new EventEmitter<any[]>();
  imagesPublication: any[] = [];

  constructor( private _http: HttpClient,
     private _usersService: UsersService,
     private _fileTransfer: FileTransfer) { 
      this.optionSelected = new EventEmitter();

  }

  optionSelectedClick ( option: any, selectedClicked: any ) {

    let values = {
      option,
      selectedClicked
    }

    this.optionSelected.emit( values );
  }

  pageReset() {
    this.paginaPosts = 0;
    this.purgarImagenes.emit( true );
  }

  getPosts( pull:boolean = false ) {

    if (pull ) {
      this.pageReset();
    }

    this.paginaPosts++; 

    //  let headers = new HttpHeaders();

    // //  headers = headers.append('Access-Control-Allow-Origin','*');
    // headers = headers.append('Authorization',`Bearer AQUI EL TOKEN`);

    let params = new HttpParams();
    params = params.append('page', `${ this.paginaPosts }`);
    
     return this._http.get(`${_URL}/api/products`, { params});



  }

  getcountrys() {
    
    return this._http.get(`${_URL}/api/countrys`);

  }

  getmonedas() {
    
    return this._http.get(`${_URL}/api/monedas`);

  }


  getPost( id ) {


    
     return this._http.get(`${_URL}/api/products/${id}`);


     
  }

   async postCreate( post: Post, imgs:any[]) {
    this.userLoged =   this._usersService.userToken;
    console.log('CREAR POST USER LOGED', this.userLoged);

    console.log('IMAGENES', imgs);

    
      await this.subirImagen( imgs);
  

    if (this.imagesPublication.length >= 1) {
      post.image = this.imagesPublication.toString();
    }

    this.imagesPublication = [];

    console.log('POST.IMG', post.image);

    return new Promise( resolve => {

    
   this._http.post(`${ _URL }/api/sellers/${this.userLoged.id}/products`, post).subscribe( resp => {
    console.log('Nueva publicacion creada',resp);



    this.nuevoPost.emit( resp['data']);
    resolve(true);
    
});

    });


  }

  async subirImagen( imgs:any[] ) {

    const options: FileUploadOptions = {
        fileKey: 'image',
        fileName: "filename",
        chunkedMode: false,
        mimeType: 'multipart/form-data',
        httpMethod: "POST"
    }
    const fileTransfer: FileTransferObject = this._fileTransfer.create();

    for ( let i=0; i < imgs.length; i++) {

    await fileTransfer.upload( imgs[i], `${ _URL }/api/image`, options)
      .then( data => {
          console.log('data', data);
          this.imagesPublication.push(data.response);
      }).catch( err => {
        
        this.imagesPublication = [];
        console.log('error en carga', err);
      });

    }

  }

  detailChannel(post:Post , imgs:any[]) {

    this.sendDetailPost.emit(post);
    this.sendDetailImages.emit(imgs);

  }


}
