import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { environment } from '../../environments/environment';
import { User, Notification } from '../interfaces/interfaces';
import { NavController } from '@ionic/angular';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';

const URL = environment.url;

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  paginaUsers = 0;
  nuevoUser = new EventEmitter<User>();

  token:string = null;
  userToken: User = null;
  imageAvatar:string = '';

  constructor(private  _http: HttpClient,
              private _storage: Storage,
              private navController: NavController ,
              private _fileTransfer: FileTransfer) { }


   login( email:string, password:string ) {

    

      const data = { email, password };

      return new Promise((resolve, reject) => {

    //  let headers = new HttpHeaders();

    //   headers = headers.append('Access-Control-Allow-Origin','*');
    //   headers = headers.append('X-Requested-With','XMLHttpRequest');
    // headers = headers.append('Content-Type','application/json');

        this._http.post(`${ URL }/api/auth/login`, data).subscribe(resp => {

          console.log(resp['access_token']);
  
          if ( resp['access_token']) {
            this.guardarToken(resp['access_token']);
            resolve();
          } else {
            this.token = null;
            this._storage.remove('token');
            this.navController.navigateRoot('/login');
           
          }
  
         }, (err) => {
           reject();
         });

      });


   }

   logout() {
     this.token = null;
     this.userToken = null;
     this._storage.remove('token');
     this.navController.navigateRoot('/login', { animated: true });
   }
   
   registro(user: User) {

    return new Promise((resolve, reject) => {

      this._http.post(`${ URL }/api/users`, user).subscribe(resp => {
        this.nuevoUser.emit( resp['data']);
        resolve();
      }, (err) => {
        reject();
      });


    });

   }

   update( user: User ) {

    return new Promise((resolve, reject) => {

     let data = {
        name: user.name,
        email: user.email,
        image: user.image,
        phone: user.phone

      };

      
      let headers = new HttpHeaders();
      // headers = headers.append('Accept','*/*');
      // headers = headers.append('Access-Control-Allow-Origin','*');
      // headers = headers.append('Content-Type','application/x-www-form-urlencoded');

      console.log('USER',user);
       this._http.put(`${ URL }/api/users/${ this.userToken.id}`,data, {headers}).subscribe(resp => {
        console.log('Actualizo',resp);
        let headers = new HttpHeaders();
        headers = headers.append('Authorization',`Bearer ${ this.token }`);
      
        this._http.post(`${ URL }/api/auth/refresh`, '', { headers }).subscribe(async resp => {
  
          if ( resp['access_token']) {

            await this.guardarToken(resp['access_token']);
            this.userToken = resp['user'];
            console.log('NUEVO_TOKEN', this.token);
            console.log('USER_UPDATED', this.userToken);

            resolve();

          } else {
            this.token = null;
            this._storage.remove('token');
           
           
          }
            
  
  
        }, (err) => {
          console.log(err);
          reject();
        });


      }, (err) => {
        reject();
      });


    });

   }

  async getUsuario() {
     if (this.userToken === null) {
        await this.validaToken();
     }

     return {...this.userToken};
   }
   
 async  guardarToken( token:string ) {
      this.token = token;
     await this._storage.set('token', token);

    //  await this.validaToken();
   }

   async cargarToken() {
      this.token = await this._storage.get('token') || null;
   }

async validaToken(): Promise<boolean> {

    await this.cargarToken();

    return new Promise<boolean>((resolve, reject) => {

      if (this.token === null) {

        // this.navController.navigateRoot('/login');
        console.log('TOKEN NULL, USUARIO NO LOGEADO');
        
        return reject();
      }

      let headers = new HttpHeaders();

      
      // headers = headers.append('Accept','*/*');
      // headers = headers.append('Accept','*/*');
      // headers = headers.append('Access-Control-Allow-Origin','*');
      // headers = headers.append('Content-Type','application/json');
      // headers = headers.append('Access-Control-Allow-Headers','Origin, Content-Type, X-Auth-Token');
      headers = headers.append('Authorization',`Bearer ${ this.token }`);
      

      this._http.post(`${ URL }/api/auth/me`,'', { headers }).subscribe(resp => {

        console.log('me',resp);
        this.userToken = resp;
        resolve();

       }, (err) => {
        this.userToken = null;
        //  this.navController.navigateRoot('/login');
        console.log('ERROR VERIFICANDO TOKEN, USUARIO NO VALIDO O TOKEN VENCIDO');
         reject();
       });

    });

}

async subirAvatar( img:any ) {

  const options: FileUploadOptions = {
      fileKey: 'image',
      fileName: "filename",
      chunkedMode: false,
      mimeType: 'multipart/form-data',
      httpMethod: "POST"
  }
  const fileTransfer: FileTransferObject = this._fileTransfer.create();


  await fileTransfer.upload( img, `${ URL }/api/image`, options)
    .then( data => {
        console.log('dataAvatarUpload', data);
        this.imageAvatar = data.response;

       let userUpdate = {
          image: this.imageAvatar
        }

        this.update(userUpdate);
     }).catch( err => {
      
      this.imageAvatar = null;
      console.log('error en carga', err);
    });


}


///////////////////////////////////////////////////////

pageReset() {

  this.paginaUsers  = 0;
  

}

getUsers( pull:boolean = false ) {

  if (pull ) {
    this.pageReset();
  }

  this.paginaUsers++; 

  //  let headers = new HttpHeaders();

  // //  headers = headers.append('Access-Control-Allow-Origin','*');
  // headers = headers.append('Authorization',`Bearer AQUI EL TOKEN`);

  let params = new HttpParams();
  params = params.append('page', `${ this.paginaUsers }`);
  
   return this._http.get(`${URL}/api/users`, { params});
}

notificationCreate( notification: Notification ) {

  return new Promise((resolve, reject) => {

    this._http.post(`${URL}/api/notification`, notification ).subscribe(resp => {

      console.log('RESP NOTIFICATION', resp );
      resolve();

    }, err => {

      console.log('ERRORNOTIFICATION', err );
      reject();
    });

  });


}


}


