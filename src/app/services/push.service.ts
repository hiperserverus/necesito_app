import { Injectable, EventEmitter } from '@angular/core';
import { OneSignal, OSNotification, OSNotificationPayload } from '@ionic-native/onesignal/ngx';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class PushService {

  mensajes: OSNotificationPayload[] = [];
  userId: any;

  pushListener = new EventEmitter<OSNotificationPayload>();

  constructor( private oneSignal: OneSignal,
              private _storage: Storage) {


    this.cargarMensajes();
               
  
  }

  async getMensajes() {
    await this.cargarMensajes();
    return [...this.mensajes];
  }

  configuracionInicial() {

        this.oneSignal.startInit('e17f039b-73e8-4f7f-9099-b7d072465c60', '733548678550');

        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);

        this.oneSignal.handleNotificationReceived().subscribe(( noti ) => {
        // do something when notification is received
        console.log('Notificacion recibida', noti);
        this.notificacionRecibida( noti );
        });

        this.oneSignal.handleNotificationOpened().subscribe(async ( noti) => {
          // do something when a notification is opened
          console.log('Notificacion Abierta', noti);
          await this.notificacionRecibida( noti.notification );
        });

        //Obtener id del subscriptor

        this.oneSignal.getIds().then(info => {
          this.userId = info.userId;
        });

        this.oneSignal.endInit();
  }

  async notificacionRecibida( noti:OSNotification ) {

    await this.cargarMensajes();

    const payload = noti.payload;

    const existePush = this.mensajes.find( mensaje => mensaje.notificationID === payload.notificationID);

    if ( existePush ) { return; }

    this.mensajes.unshift( payload );
    this.pushListener.emit( payload );

    this.guardarMensajes();
  }

  guardarMensajes() {
    this._storage.set('mensajes', this.mensajes);
  }

  async cargarMensajes() {
    // this._storage.remove('mensajes');
    this.mensajes = await this._storage.get('mensajes') || [];

    return this.mensajes;
  }

  async borrarMensajes() {
   await  this._storage.remove('mensajes');
    this.mensajes = [];
    this.guardarMensajes();

  }


}
