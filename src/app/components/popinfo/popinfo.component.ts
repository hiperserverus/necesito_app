import { Component, OnInit, Input } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { UsersService } from '../../services/users.service';
import { User } from '../../interfaces/interfaces';

@Component({
  selector: 'app-popinfo',
  templateUrl: './popinfo.component.html',
  styleUrls: ['./popinfo.component.scss'],
})
export class PopinfoComponent implements OnInit {

  @Input() level;
  @Input() status;



  items = [
    {
      route: '/timeline',
      icon: 'paper',
      name: 'Noticias',
      level: 'any',
      status: 'any'
  },
  {
    route: '/notifications',
    icon: 'md-globe',
    name: 'Notificaciones',
    level: 'any',
    status: 'any'
    
},
  
 {
      route: '/profile',
      icon: 'person',
      name: 'Perfil',
      level: 'any',
      status: 'loged'
  },
  {
    route: '/list',
    icon: 'body',
    name: 'Lista de usuarios',
    level: 'admin',
    status: 'loged'
},
  {
    route: '/publication-create',
    icon: 'create',
    name: 'Crear publicación',
    level: 'admin',
    status: 'loged'
},
{
  route: '/notification-create',
  icon: 'ios-send',
  name: 'Crear notificación',
  level: 'admin',
  status: 'loged'
},
{
  route: '/login',
  icon: 'contact',
  name: 'Entrar',
  level: 'any',
  status: 'unloged'
},
{
  route: '/login',
  icon: 'power',
  name: 'Salir',
  level: 'any',
  status: 'loged'
}

];

  

  constructor( private popoverController: PopoverController,
    private _usersService: UsersService ) { }

  ngOnInit() {







  }

  onClick( index ) {


    if( this.items[index].name === 'Salir' ) {
      this._usersService.logout();
    }

  this.popoverController.dismiss();

  }

  Salir() {
    this._usersService.logout();
  }

}
